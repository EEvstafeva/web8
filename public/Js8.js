function saveLocalStorage() {
    localStorage.setItem("name", $("#name").val());
    localStorage.setItem("email", $("#email").val());
    localStorage.setItem("message", $("#message").val());
    localStorage.setItem("done", $("#done").prop("checked"));
}
function loadLocalStorage() {
    if (localStorage.getItem("name") !== null) {
        $("#name").val(localStorage.getItem("name"));
    }
    if (localStorage.getItem("email") !== null) {
        $("#email").val(localStorage.getItem("email"));
    }
    if (localStorage.getItem("message") !== null) {
        $("#message").val(localStorage.getItem("message"));
    }
    if (localStorage.getItem("policy") !== null) {
        $("#done").prop("checked", localStorage.getItem("done") === "true");
        if ($("#done").prop("checked")) {
            $("#sendBut").removeAttr("disabled");

        }
    }
}
function clear() {
    localStorage.clear();
    $("#name").val("");
    $("#email").val("");
    $("#message").val("");
    $("#done").val(false);
}

$(document).ready(function () {
    loadLocalStorage();
    $("#openBut").click(function () {
        $(".fixed-overlay").css("display", "flex");
        history.pushState(true, "", "./form");
    });
});
$("#closeBut").click(function () {
    $(".fixed-overlay").css("display", "none");
    history.pushState(false, "", ".");
});
$("#form").submit(function (e) {
    e.preventDefault();
    $(".fixed-overlay").css("display", "none");
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "https://formcarry.com/s/S9Et0yo4eE",
        data: $(this).serialize(),
        success: function (response) {
            if (response.status === "success") {
                alert("Success! ");
                clear();
            } else {
                alert("Error: " + response.message);
            }
        }
    });
});
$("#done").change(function () {
    if (this.checked) {
        $("#sendBut").removeAttr("disabled");
    } else {
        $("#sendBut").attr("disabled", "");
    }
});
$("#form").change(saveLocalStorage);
window.onpopstate = function (event) { 
    if (event.state) {
        $(".fixed-overlay").css("display", "flex");
    } else {
        $(".fixed-overlay").css("display", "none");
    }
};

